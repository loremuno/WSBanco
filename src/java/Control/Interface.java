/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import DTO.DTOCliente;
import DTO.DTOCuenta;
import DTO.DTODebitoEstadoBanco;
import DTO.DTOSaldoBanco;
import java.util.List;

/**
 *
 * @author Lorenzo
 */
public interface Interface {
    DTOSaldoBanco obtenerSaldoCuenta(int numeroCuenta);
    DTODebitoEstadoBanco generarDebito(double importeOperacion, int numeroCuenta);
    List<DTOCuenta> obtenerCuentas(String numeroCliente);
    List<DTOCuenta> obtenerDatosBancarios(String numeroCliente);
    List<DTOCliente> obtenerClientes();
}
