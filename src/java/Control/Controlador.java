/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import Control.FabricaExpertos;
import DTO.DTOCliente;
import DTO.DTOCuenta;
import DTO.DTODebitoEstadoBanco;
import DTO.DTOSaldoBanco;
import java.util.List;

/**
 *
 * @author Lorenzo
 */
public class Controlador {
    private Decorador decorador;
    
    public void iniciarT(){
      this.decorador = (Decorador) FabricaExpertos.getInstancia().crearExperto("Banco"); 
      decorador.iniciarT();
    }
     public DTOSaldoBanco obtenerSaldoCuenta(int numeroCuenta){
         return decorador.obtenerSaldoCuenta(numeroCuenta);
    }
    public List<DTOCuenta> obtenerCuentas(String numeroCliente){
        return decorador.obtenerCuentas(numeroCliente);
    }
    public List<DTOCuenta> obtenerDatosBancarios(String numeroCliente){
        return decorador.obtenerDatosBancarios(numeroCliente);
    }
    public List<DTOCliente> obtenerClientes(){
         return decorador.obtenerClientes();
    }
    public DTODebitoEstadoBanco generarDebito(double importeOperacion, int numeroCuenta) {
        return decorador.generarDebito(importeOperacion,numeroCuenta);
    }

}
