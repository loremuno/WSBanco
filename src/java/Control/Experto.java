/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import DTO.DTOCliente;
import DTO.DTOCriterio;
import DTO.DTOCuenta;
import DTO.DTODebitoEstadoBanco;
import DTO.DTOSaldoBanco;
import DTO.DTOTipoCuenta;
import Entidades.Cliente;
import Entidades.Cuenta;
import Persistencia.FachadaPersistencia;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lorenzo
 */
public class Experto implements Interface{
    
    @Override
     public DTOSaldoBanco obtenerSaldoCuenta(int numeroCuenta){
        List<DTOCriterio> listaDTOCriterio = new ArrayList<>();
        DTOCriterio dtoCriterio = new DTOCriterio();
        dtoCriterio.setAtributo("numeroCuenta");
        dtoCriterio.setOperador("=");
        dtoCriterio.setValor(numeroCuenta);
        listaDTOCriterio.add(dtoCriterio);
            
        DTOCriterio dtoCriterio1 = new DTOCriterio();
        dtoCriterio1.setAtributo("fechaInhabilitacionCuenta");
        dtoCriterio1.setOperador("is null");
        dtoCriterio1.setValor(null);
        listaDTOCriterio.add(dtoCriterio1);
        
        try{
            Cuenta cuenta = (Cuenta)(Object)(FachadaPersistencia.getInstancia().buscar("Cuenta", listaDTOCriterio).get(0));
            DTOSaldoBanco dtoSaldo = new DTOSaldoBanco();
            dtoSaldo.setSaldo(cuenta.getSaldoCuenta());
            return dtoSaldo;
        }
        catch(IndexOutOfBoundsException e){
            return null;
        }
    }
    
     @Override
     public List<DTOCuenta> obtenerCuentas(String numeroCliente){
         
        List<DTOCriterio> listaDTOCriterio = new ArrayList<>();
        DTOCriterio dtoCriterio = new DTOCriterio();
        dtoCriterio.setAtributo("numeroCliente");
        dtoCriterio.setOperador("=");
        dtoCriterio.setValor(numeroCliente);
        listaDTOCriterio.add(dtoCriterio);
                
        try{
            Cliente cliente = (Cliente)(Object)(FachadaPersistencia.getInstancia().buscar("Cliente", listaDTOCriterio).get(0));

            List<DTOCriterio> listaDTOCriterio1 = new ArrayList<>();
            DTOCriterio dtoCriterio1 = new DTOCriterio();
            dtoCriterio1.setAtributo("cliente");
            dtoCriterio1.setOperador("=");
            dtoCriterio1.setValor(cliente);
            listaDTOCriterio1.add(dtoCriterio1);
            
            List<Cuenta> cuentas = (List<Cuenta>)(Object)(FachadaPersistencia.getInstancia().buscar("Cuenta", listaDTOCriterio1));
            List<DTOCuenta> listaDtoCuenta = new ArrayList<>();
            for(Cuenta item:cuentas){
                DTOCuenta  dtoCuenta = new DTOCuenta();
            
                dtoCuenta.setNumeroCuenta(item.getNumeroCuenta());
                dtoCuenta.setSaldoCuenta(item.getSaldoCuenta());
                dtoCuenta.setFechaHabilitacionCuenta(item.getFechaHabilitacionCuenta());
                dtoCuenta.setFechaInhabilitacionCuenta(item.getFechaInhabilitacionCuenta());
                
                DTOTipoCuenta dtoTipoCuenta = new DTOTipoCuenta();
                dtoTipoCuenta.setCodigoTipoCuenta(item.getTipoCuenta().getCodigoTipoCuenta());
                dtoTipoCuenta.setFechaHabilitacionTipoCuenta(item.getTipoCuenta().getFechaHabilitacionTipoCuenta());
                dtoTipoCuenta.setFechaInhabilitacionTipoCuenta(item.getTipoCuenta().getFechaInhabilitacionTipoCuenta());
                dtoTipoCuenta.setNombreTipoCuenta(item.getTipoCuenta().getNombreTipoCuenta());
                dtoCuenta.setTipoCuenta(dtoTipoCuenta);
                
//                DTOCliente dtoCliente = new DTOCliente();
//                dtoCliente.setApellidoCliente(item.getCliente().getapellidoCliente());
//                dtoCliente.setDniCliente(item.getCliente().getdniCliente());
//                dtoCliente.setFechaHabilitacionCliente(item.getCliente().getfechaHabilitacionCliente());
//                dtoCliente.setFechaInhabilitacionCliente(item.getCliente().getfechaInhabilitacionCliente());
//                dtoCliente.setNombreCliente(item.getCliente().getnombreCliente());
//                dtoCliente.setNumeroCliente(item.getCliente().getnumeroCliente());
//                dtoCuenta.setCliente(dtoCliente);
                
                listaDtoCuenta.add(dtoCuenta);
            }
            
            return listaDtoCuenta;
        }
        catch(IndexOutOfBoundsException e){
            return null;
        }
     }
     @Override
     public List<DTOCuenta> obtenerDatosBancarios(String numeroCliente){
       List<DTOCriterio> listaDTOCriterio = new ArrayList<>();
        DTOCriterio dtoCriterio = new DTOCriterio();
        dtoCriterio.setAtributo("numeroCliente");
        dtoCriterio.setOperador("=");
        dtoCriterio.setValor(numeroCliente);
        listaDTOCriterio.add(dtoCriterio);
                
        try{
            Cliente cliente = (Cliente)(Object)(FachadaPersistencia.getInstancia().buscar("Cliente", listaDTOCriterio).get(0));

            List<DTOCriterio> listaDTOCriterio1 = new ArrayList<>();
            DTOCriterio dtoCriterio1 = new DTOCriterio();
            dtoCriterio1.setAtributo("cliente");
            dtoCriterio1.setOperador("=");
            dtoCriterio1.setValor(cliente);
            listaDTOCriterio1.add(dtoCriterio1);
            
            List<Cuenta> cuentas = (List<Cuenta>)(Object)(FachadaPersistencia.getInstancia().buscar("Cuenta", listaDTOCriterio1));
            List<DTOCuenta> listaDtoCuenta = new ArrayList<>();
            for(Cuenta item:cuentas){
                DTOCuenta  dtoCuenta = new DTOCuenta();
            
                dtoCuenta.setNumeroCuenta(item.getNumeroCuenta());
                dtoCuenta.setSaldoCuenta(item.getSaldoCuenta());
                dtoCuenta.setFechaHabilitacionCuenta(item.getFechaHabilitacionCuenta());
                dtoCuenta.setFechaInhabilitacionCuenta(item.getFechaInhabilitacionCuenta());
                
                DTOTipoCuenta dtoTipoCuenta = new DTOTipoCuenta();
                dtoTipoCuenta.setCodigoTipoCuenta(item.getTipoCuenta().getCodigoTipoCuenta());
                dtoTipoCuenta.setFechaHabilitacionTipoCuenta(item.getTipoCuenta().getFechaHabilitacionTipoCuenta());
                dtoTipoCuenta.setFechaInhabilitacionTipoCuenta(item.getTipoCuenta().getFechaInhabilitacionTipoCuenta());
                dtoTipoCuenta.setNombreTipoCuenta(item.getTipoCuenta().getNombreTipoCuenta());
                dtoCuenta.setTipoCuenta(dtoTipoCuenta);
                
                DTOCliente dtoCliente = new DTOCliente();
                dtoCliente.setApellidoCliente(item.getCliente().getapellidoCliente());
                dtoCliente.setDniCliente(item.getCliente().getdniCliente());
                dtoCliente.setFechaHabilitacionCliente(item.getCliente().getfechaHabilitacionCliente());
                dtoCliente.setFechaInhabilitacionCliente(item.getCliente().getfechaInhabilitacionCliente());
                dtoCliente.setNombreCliente(item.getCliente().getnombreCliente());
                dtoCliente.setNumeroCliente(item.getCliente().getnumeroCliente());
                dtoCuenta.setCliente(dtoCliente);
                
                listaDtoCuenta.add(dtoCuenta);
            }
            
            return listaDtoCuenta;
        }
        catch(IndexOutOfBoundsException e){
            return null;
        }
    }
     
    @Override
    public List<DTOCliente> obtenerClientes(){
        List<DTOCliente> lista = new ArrayList<>();
        try{
            List<Cliente> listaCliente = (List<Cliente>)(Object)(FachadaPersistencia.getInstancia().buscar("Cliente", null));
            for(Cliente item : listaCliente){
                DTOCliente dto = new DTOCliente();
                dto.setApellidoCliente(item.getapellidoCliente());
                dto.setDniCliente(item.getdniCliente());
                dto.setFechaInhabilitacionCliente(item.getfechaInhabilitacionCliente());
                dto.setNumeroCliente(item.getnumeroCliente());
                dto.setNombreCliente(item.getnombreCliente());
                dto.setFechaHabilitacionCliente(item.getfechaHabilitacionCliente());
                lista.add(dto);
            }
            return lista;
        }
        catch(IndexOutOfBoundsException e){
            return null;
        }
    }
     
    @Override
    public DTODebitoEstadoBanco generarDebito(double importeOperacion, int numeroCuenta){
        List<DTOCriterio> listaDTOCriterio = new ArrayList<>();
        DTOCriterio dtoCriterio = new DTOCriterio();
        dtoCriterio.setAtributo("numeroCuenta");
        dtoCriterio.setOperador("=");
        dtoCriterio.setValor(numeroCuenta);
        listaDTOCriterio.add(dtoCriterio);

        try{
            Cuenta cuenta = (Cuenta)(Object)(FachadaPersistencia.getInstancia().buscar("Cuenta", listaDTOCriterio).get(0));
            DTODebitoEstadoBanco dtoDebitoEstado = new DTODebitoEstadoBanco();
            if ((cuenta.getSaldoCuenta() - importeOperacion) >= 0) {
                dtoDebitoEstado.setCodigoEstado(01);
                dtoDebitoEstado.setNombreEstado("Debitado");
                dtoDebitoEstado.setMotivoEstado("Su pago se débito con exito");
            }
            else{
                dtoDebitoEstado.setCodigoEstado(02);
                dtoDebitoEstado.setNombreEstado("No debitado");
                dtoDebitoEstado.setMotivoEstado("No se ha podido debitar de su cuenta.");
            }
            cuenta.setSaldoCuenta(cuenta.getSaldoCuenta() - importeOperacion);
            FachadaPersistencia.getInstancia().guardar(cuenta);
            return dtoDebitoEstado;
        }
        catch(IndexOutOfBoundsException e){
            return null;
        }
    }
}
