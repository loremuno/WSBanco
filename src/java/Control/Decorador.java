/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import DTO.DTOCliente;
import DTO.DTOCuenta;
import DTO.DTODebitoEstadoBanco;
import DTO.DTOSaldoBanco;
import Persistencia.FachadaInterna;
import java.util.List;

/**
 *
 * @author Lorenzo
 */
public class Decorador extends Experto{
    public void iniciarT(){        
        FachadaInterna.getInstancia().iniciarTransaccion();
    }
    public DTOSaldoBanco obtenerSaldoCuenta(int numeroCuenta){
        return super.obtenerSaldoCuenta(numeroCuenta);
    }
    public List<DTOCuenta> obtenerCuentas(String numeroCliente){
        return super.obtenerCuentas(numeroCliente);
    }
    public List<DTOCuenta> obtenerDatosBancarios(String numeroCliente){
        return super.obtenerDatosBancarios(numeroCliente);
    }
    public List<DTOCliente> obtenerClientes(){
         return super.obtenerClientes();
    }
    public DTODebitoEstadoBanco generarDebito(double importeOperacion, int numeroCuenta){
        try{
            DTODebitoEstadoBanco dto = super.generarDebito(importeOperacion,numeroCuenta);
            FachadaInterna.getInstancia().finalizarTransaccion();
            return dto;
        }
        catch(Exception e){
            return null;
        }
    }
}
