/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WS;

import Control.Controlador;
import DTO.DTOCliente;
import DTO.DTOCuenta;
import DTO.DTODebitoEstadoBanco;
import DTO.DTOSaldoBanco;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Lorenzo
 */
@WebService(serviceName = "Banco")
public class Banco {

   Controlador controlador = new Controlador();

    /**
     * Web service operation
     */
    @WebMethod(operationName = "obtenerSaldoCuenta")
    public DTOSaldoBanco obtenerSaldoCuenta(@WebParam(name = "numeroCuenta") int numeroCuenta) {
        //TODO write your implementation code here:
        controlador.iniciarT();
        return controlador.obtenerSaldoCuenta(numeroCuenta);
    }
    
    @WebMethod(operationName = "obtenerCuentas")
        public List<DTOCuenta> obtenerCuentas(@WebParam(name = "numeroCliente") String numeroCliente) {
        //TODO write your implementation code here:
        controlador.iniciarT();
        return controlador.obtenerCuentas(numeroCliente);
    }
    
    @WebMethod(operationName = "obtenerDatosBancarios")
    public List<DTOCuenta> obtenerDatosBancarios(String numeroCliente) {
        //TODO write your implementation code here:
        controlador.iniciarT();
        return controlador.obtenerDatosBancarios(numeroCliente);
    }
    @WebMethod(operationName = "obtenerClientes")
    public List<DTOCliente> obtenerClientes() {
        //TODO write your implementation code here:
        controlador.iniciarT();
        return controlador.obtenerClientes();
    }
    /**
     * Web service operation
     */
    @WebMethod(operationName = "generarDebito")
    public DTODebitoEstadoBanco generarDebito(@WebParam(name = "numeroCuenta") int numeroCuenta, @WebParam(name = "importeOperacion") double importeOperacion) {
        //TODO write your implementation code here:
        controlador.iniciarT();
        return controlador.generarDebito(importeOperacion,numeroCuenta);
    }
}
