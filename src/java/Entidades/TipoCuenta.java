package Entidades;

import java.util.Date;

/**
 * @author Fernando
 * @version 1.0
 * @created 07-Sep-2017 7:07:39 PM
 */
public class TipoCuenta extends Entidad{

	private int codigoTipoCuenta;
	private Date fechaHabilitacionTipoCuenta;
	private Date fechaInhabilitacionTipoCuenta;
	private String nombreTipoCuenta;

    public int getCodigoTipoCuenta() {
        return codigoTipoCuenta;
    }

    public void setCodigoTipoCuenta(int codigoTipoCuenta) {
        this.codigoTipoCuenta = codigoTipoCuenta;
    }

    public Date getFechaHabilitacionTipoCuenta() {
        return fechaHabilitacionTipoCuenta;
    }

    public void setFechaHabilitacionTipoCuenta(Date fechaHabilitacionTipoCuenta) {
        this.fechaHabilitacionTipoCuenta = fechaHabilitacionTipoCuenta;
    }

    public Date getFechaInhabilitacionTipoCuenta() {
        return fechaInhabilitacionTipoCuenta;
    }

    public void setFechaInhabilitacionTipoCuenta(Date fechaInhabilitacionTipoCuenta) {
        this.fechaInhabilitacionTipoCuenta = fechaInhabilitacionTipoCuenta;
    }

    public String getNombreTipoCuenta() {
        return nombreTipoCuenta;
    }

    public void setNombreTipoCuenta(String nombreTipoCuenta) {
        this.nombreTipoCuenta = nombreTipoCuenta;
    }

    
      public TipoCuenta(){}
      public TipoCuenta(int OID){
          super(OID);
          
      }
                @Override
    public int getOID() {
        return super.OID;
    }

    /**
     *
     * @param OID
     */
    @Override
    public void setOID(int OID) {
        super.OID = OID;
    }
}//end TipoCuenta