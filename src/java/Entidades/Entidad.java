package Entidades;

public abstract class Entidad
{

    protected int OID;

    public abstract int getOID ();
    public abstract void setOID (int OID);

    /**
     *
     */
    public Entidad() {
    }

    /**
     *
     * @param OID
     */
    public Entidad(int OID) {
        this.OID = OID;
    }
    

}

