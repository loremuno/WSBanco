package Entidades;

import java.util.Date;

/**
 * @author Fernando
 * @version 1.0
 * @created 07-Sep-2017 7:07:07 PM
 */
public class Cuenta extends Entidad{

	private Date fechaHabilitacionCuenta;
	private Date fechaInhabilitacionCuenta;
	private int numeroCuenta;
        private Cliente cliente;
        private TipoCuenta tipoCuenta;
        private double saldoCuenta;
    public Cuenta(){
        
        
    }    
        
    public Cuenta(int OID,Date fechaHabilitacionCuenta,Date fechaInhabilitacionCuenta,int numeroCuenta,Cliente cliente,TipoCuenta tipocuenta){
        
        this.OID = OID;
        this.cliente = cliente;
        this.fechaHabilitacionCuenta = fechaHabilitacionCuenta;
        this.fechaInhabilitacionCuenta = fechaInhabilitacionCuenta;
        this.numeroCuenta = numeroCuenta;
        this.tipoCuenta = tipoCuenta;
        
    }

    public void setSaldoCuenta(double saldoCuenta) {
        this.saldoCuenta = saldoCuenta;
    }

    public double getSaldoCuenta() {
        return saldoCuenta;
    }

    public Date getFechaHabilitacionCuenta() {
        return fechaHabilitacionCuenta;
    }

    public void setFechaHabilitacionCuenta(Date fechaHabilitacionCuenta) {
        this.fechaHabilitacionCuenta = fechaHabilitacionCuenta;
    }

    public Date getFechaInhabilitacionCuenta() {
        return fechaInhabilitacionCuenta;
    }

    public void setFechaInhabilitacionCuenta(Date fechaInhabilitacionCuenta) {
        this.fechaInhabilitacionCuenta = fechaInhabilitacionCuenta;
    }

    public int getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(int numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }


    public TipoCuenta getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(TipoCuenta tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    
                @Override
    public int getOID() {
        return super.OID;
    }

    /**
     *
     * @param OID
     */
    @Override
    public void setOID(int OID) {
        super.OID = OID;
    }
}//end Cuenta