/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.Date;

/**
 *
 * @author Lorenzo
 */
public class DTOCuenta {
    private Date fechaHabilitacionCuenta;
	private Date fechaInhabilitacionCuenta;
	private int numeroCuenta;
        private DTOCliente cliente;
        private DTOTipoCuenta tipoCuenta;
        private double saldoCuenta;

    public DTOCuenta() {
    }

    public Date getFechaHabilitacionCuenta() {
        return fechaHabilitacionCuenta;
    }

    public Date getFechaInhabilitacionCuenta() {
        return fechaInhabilitacionCuenta;
    }

    public int getNumeroCuenta() {
        return numeroCuenta;
    }

    public DTOCliente getCliente() {
        return cliente;
    }

    public DTOTipoCuenta getTipoCuenta() {
        return tipoCuenta;
    }

    public double getSaldoCuenta() {
        return saldoCuenta;
    }

    public void setFechaHabilitacionCuenta(Date fechaHabilitacionCuenta) {
        this.fechaHabilitacionCuenta = fechaHabilitacionCuenta;
    }

    public void setFechaInhabilitacionCuenta(Date fechaInhabilitacionCuenta) {
        this.fechaInhabilitacionCuenta = fechaInhabilitacionCuenta;
    }

    public void setNumeroCuenta(int numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public void setCliente(DTOCliente cliente) {
        this.cliente = cliente;
    }

    public void setTipoCuenta(DTOTipoCuenta tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    public void setSaldoCuenta(double saldoCuenta) {
        this.saldoCuenta = saldoCuenta;
    }
        
}
