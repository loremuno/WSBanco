/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author Lorenzo
 */
public class DTODebitoEstadoBanco {
    private int codigoEstado;
    private String motivoEstado;
    private String nombreEstado;

    public DTODebitoEstadoBanco() {
    }

    public int getCodigoEstado() {
        return codigoEstado;
    }

    public String getMotivoEstado() {
        return motivoEstado;
    }

    public String getNombreEstado() {
        return nombreEstado;
    }

    public void setCodigoEstado(int codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public void setMotivoEstado(String motivoEstado) {
        this.motivoEstado = motivoEstado;
    }

    public void setNombreEstado(String nombreEstado) {
        this.nombreEstado = nombreEstado;
    }
   
}
