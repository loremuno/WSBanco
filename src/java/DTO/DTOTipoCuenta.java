/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.Date;

/**
 *
 * @author Lorenzo
 */
public class DTOTipoCuenta {
    private int codigoTipoCuenta;
	private Date fechaHabilitacionTipoCuenta;
	private Date fechaInhabilitacionTipoCuenta;
	private String nombreTipoCuenta;

    public DTOTipoCuenta() {
    }

    public int getCodigoTipoCuenta() {
        return codigoTipoCuenta;
    }

    public Date getFechaHabilitacionTipoCuenta() {
        return fechaHabilitacionTipoCuenta;
    }

    public Date getFechaInhabilitacionTipoCuenta() {
        return fechaInhabilitacionTipoCuenta;
    }

    public String getNombreTipoCuenta() {
        return nombreTipoCuenta;
    }

    public void setCodigoTipoCuenta(int codigoTipoCuenta) {
        this.codigoTipoCuenta = codigoTipoCuenta;
    }

    public void setFechaHabilitacionTipoCuenta(Date fechaHabilitacionTipoCuenta) {
        this.fechaHabilitacionTipoCuenta = fechaHabilitacionTipoCuenta;
    }

    public void setFechaInhabilitacionTipoCuenta(Date fechaInhabilitacionTipoCuenta) {
        this.fechaInhabilitacionTipoCuenta = fechaInhabilitacionTipoCuenta;
    }

    public void setNombreTipoCuenta(String nombreTipoCuenta) {
        this.nombreTipoCuenta = nombreTipoCuenta;
    }
        
}
