/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.Date;

/**
 *
 * @author Lorenzo
 */
public class DTOCliente {
    private String apellidoCliente;
	private int dniCliente;
	private Date fechaHabilitacionCliente;
	private Date fechaInhabilitacionCliente;
	private String nombreCliente;
	private String numeroCliente;

    public DTOCliente() {
    }

    public String getApellidoCliente() {
        return apellidoCliente;
    }

    public int getDniCliente() {
        return dniCliente;
    }

    public Date getFechaHabilitacionCliente() {
        return fechaHabilitacionCliente;
    }

    public Date getFechaInhabilitacionCliente() {
        return fechaInhabilitacionCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public String getNumeroCliente() {
        return numeroCliente;
    }

    public void setApellidoCliente(String apellidoCliente) {
        this.apellidoCliente = apellidoCliente;
    }

    public void setDniCliente(int dniCliente) {
        this.dniCliente = dniCliente;
    }

    public void setFechaHabilitacionCliente(Date fechaHabilitacionCliente) {
        this.fechaHabilitacionCliente = fechaHabilitacionCliente;
    }

    public void setFechaInhabilitacionCliente(Date fechaInhabilitacionCliente) {
        this.fechaInhabilitacionCliente = fechaInhabilitacionCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public void setNumeroCliente(String numeroCliente) {
        this.numeroCliente = numeroCliente;
    }
        
        
}
