/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistencia;

import DTO.DTOCriterio;
import java.util.List;

/**
 *
 * @author Lorenzo
 */
public class FachadaPersistencia {
     private static FachadaPersistencia instancia;

    public FachadaPersistencia ()
    {
    }

    public static FachadaPersistencia getInstancia ()
    {
        if (instancia == null)
        {
            instancia = new FachadaPersistencia();
        }
        return instancia;
    }

    /**
     *
     * @param claseABuscar
     * @param criterioList
     * @return
     */
    public List<Object> buscar (String claseABuscar, List<DTOCriterio> criterioList)
    {
        return FachadaInterna.getInstancia().buscar(claseABuscar, criterioList);
    }

    public void guardar (Object objeto)
    {
        FachadaInterna.getInstancia().guardar(objeto);
    }
    
    public void eliminar (Object objeto){
        FachadaInterna.getInstancia().eliminar(objeto);
    }
    
}
